Use this directory to override template, theme and \[pre\]process functions.

Please refer to the @link registry Theme Registry @endlink topic for more info.

### Deprecations
Outdated implementations are not removed immediately allowing you to update
your projects and migrate to new components without breaking your site pages.

#### Templates
Outdated templates are marked with `@deprecated` notices in the next version and planned to
be removed in the future releases.

##### OpenY Repeat
- templates/openy_repeat/openy-repeat-schedule-dashboard.html.twig
- templates/openy_repeat/openy-repeat-schedule-dashboard--table.html.twig
- templates/openy_repeat/openy-repeat-schedule-dashboard--sidebar.html.twig

##### OpenY Membership Calculator
- templates/form/form-element-membership-type.html.twig
- templates/node/branch/node--branch--calc_summary.html.twig
- templates/node/membership/node--membership--calc_preview.html.twig
- templates/node/membership/node--membership--calc_summary.html.twig
- templates/node/membership/node--membership--full.html.twig
- templates/openy_calc/openy-calc-form-header.html.twig
- templates/openy_calc/openy-calc-form-summary.html.twig
